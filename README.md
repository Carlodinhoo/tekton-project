# TekTon Project

Project to complete the Tekton test

## Getting started

To run the project, follow the next steps:

-	Clone the project
-	Execute: mvn install
-	mvn spring-boot:run

## Project Architecture

The project has 6 packages:

- Configuration: Classes to handle the project profile.
- Controllers: Classes to handle all the request sent it by the user.
- Models: Classes that define the entities of our application.
- Reposiroty: Class to allow us to use methods from JPA.
- Services: Classes that define the actions of each method.
- Util: Class that helps with the return object.


## External Service by Retool

- (GET): https://retoolapi.dev/bIlvSO/products
- (GET): https://retoolapi.dev/bIlvSO/products/:id
- (POST): https://retoolapi.dev/bIlvSO/products/
- (PUT): https://retoolapi.dev/bIlvSO/products/:id
- (DELETE): https://retoolapi.dev/bIlvSO/products/:id

## API REST

- (GET): http://localhost:8080/products
- (GET): http://localhost:8080/products/:id
- (POST): http://localhost:8080/products/
- (PUT): http://localhost:8080/products/:id
- (DELETE): http://localhost:8080/products/:id

## Authors
Juan Carlos Garduño Vargas
