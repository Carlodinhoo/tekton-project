package com.tekton.test.controllers;

import com.tekton.test.configuration.RetoolConstans;
import com.tekton.test.models.Product;
import com.tekton.test.services.ProductService;
import com.tekton.test.util.ResponseGeneric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    RestTemplate restTemplate;

    Logger logger = LoggerFactory.getLogger(ProductController.class);

    @GetMapping()
    public ResponseGeneric getProducts(){
        Long a = System.currentTimeMillis();
        ResponseGeneric response = new ResponseGeneric();

        try{
            ResponseEntity<Product[]> responseEntity = restTemplate.getForEntity(RetoolConstans.retoolAPI, Product[].class);
            Product[] objects = responseEntity.getBody();
            response.setStatus("200");
            response.setMessage("success");
            response.setResponse(objects);
        }catch(Exception error){
            response.setStatus("400");
            response.setMessage("Sorry and unexpected error occurred, please verify and try again.");
            response.setResponse(error);
        }finally {
            Long b = System.currentTimeMillis();
            logger.info("The process getProducts took "+(b-a)+ " milliseconds.");
        }


        return response;
    }

    @PostMapping()
    public ResponseGeneric saveProduct(@RequestBody Product product){
        Long a = System.currentTimeMillis();
        ResponseGeneric response = new ResponseGeneric();

        try{
            ResponseEntity<Product> responseEntity = restTemplate.postForEntity(RetoolConstans.retoolAPI, product, Product.class);
            Product objects = responseEntity.getBody();
            this.productService.saveProduct(product);
            response.setStatus("200");
            response.setMessage("success");
            response.setResponse(objects);
        }catch(Exception error){
            response.setStatus("400");
            response.setMessage("Sorry and unexpected error occurred, please verify and try again.");
            response.setResponse(error);
        }finally {
            Long b = System.currentTimeMillis();
            logger.info("The process saveProduct took "+(b-a)+ " milliseconds.");
        }

        return response;
    }

    @PutMapping(path = "/{id}")
    public ResponseGeneric updateProduct(@RequestBody Product product, @PathVariable("id") Long id){
        Long a = System.currentTimeMillis();
        ResponseGeneric response = new ResponseGeneric();
        try{
            restTemplate.put(RetoolConstans.retoolAPI+"/"+id, product);
            this.productService.saveProduct(product);
            response.setStatus("200");
            response.setMessage("success");
            response.setResponse("The product "+product.getName()+ " has been updated");
        }catch(Exception e){
            response.setStatus("400");
            response.setMessage("Sorry and unexpected error occurred, please verify and try again.");
            response.setResponse(e);
        }finally {
            Long b = System.currentTimeMillis();
            logger.info("The process updateProduct took "+(b-a)+ " milliseconds.");
        }

        return response;
    }

    @GetMapping(path = "/{id}")
    public ResponseGeneric getProductById (@PathVariable("id") Long id){
        Long a = System.currentTimeMillis();
        ResponseGeneric response = new ResponseGeneric();
        Product product;
        try{
            ResponseEntity<Product> responseEntity = restTemplate.getForEntity(RetoolConstans.retoolAPI+"/"+id, Product.class);
            product = responseEntity.getBody();
            response.setStatus("200");
            response.setMessage("success");
            response.setResponse(product);
        }catch(Exception error){
            response.setStatus("400");
            response.setMessage("Sorry and unexpected error occurred, please verify and try again.");
            response.setResponse(error);
        }finally {
            Long b = System.currentTimeMillis();
            logger.info("The process getProductById took "+(b-a)+ " milliseconds.");
        }
        return response;
    }

    @GetMapping("/query")
    public Product getProductByName(@RequestParam("name") String name){
        Long a = System.currentTimeMillis();
        Product item = this.productService.getByName(name);
        Long b = System.currentTimeMillis();
        logger.info("The process getProductByName took "+(b-a)+ " milliseconds.");
        return item;
    }

    @DeleteMapping(path = "/{id}")
    public ResponseGeneric deleteProductById(@PathVariable("id") Long id){
        Long a = System.currentTimeMillis();
        ResponseGeneric response = new ResponseGeneric();
        try{
            restTemplate.delete(RetoolConstans.retoolAPI+"/"+id);
            response.setStatus("200");
            response.setMessage("success");
            response.setResponse("The product "+id+ " has been deleted");
        }catch(Exception e){
            response.setStatus("400");
            response.setMessage("Sorry and unexpected error occurred, please verify and try again.");
            response.setResponse(e);
        }finally {
            Long b = System.currentTimeMillis();
            logger.info("The process deleteProductById took "+(b-a)+ " milliseconds.");
        }

        return response;

    }

}
