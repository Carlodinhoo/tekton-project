package com.tekton.test.repositories;

import com.tekton.test.models.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepo extends CrudRepository<Product, Long> {

    public abstract Product findByName(String name);
}
