package com.tekton.test.services;

import com.tekton.test.models.Product;
import com.tekton.test.repositories.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepo productRepo;

    public ArrayList<Product> getProducts () {
        return (ArrayList<Product>) productRepo.findAll();
    }

    public Product saveProduct( Product product ) {
        return productRepo.save(product);
    }

    public Optional<Product> getById(Long id){
        return productRepo.findById(id);
    }

    public Product getByName(String name){
        return productRepo.findByName(name);
    }

    public boolean deleteProduct(Long id){
        try{
            productRepo.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }
}
